//
//  ViewController.h
//  QRCodeAndDownLoadTask
//
//  Created by Mounika Nerella on 9/14/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<NSURLSessionDownloadDelegate>

@end

