//
//  ViewController.m
//  QRCodeAndDownLoadTask
//
//  Created by Mounika Nerella on 9/14/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong,nonatomic) NSURLSession *backgroundSession;
@property (strong,nonatomic) NSURLSessionDownloadTask *downloadTask;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    _progressView.transform = transform;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];//need to write here.
    
    self.backgroundSession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    double value = (double)totalBytesWritten/(double)totalBytesExpectedToWrite;
    [_progressView setProgress:value animated:YES];

}

    /* Sent when a download has been resumed. If a download failed with an
     * error, the -userInfo dictionary of the error will contain an
     * NSURLSessionDownloadTaskResumeData key, whose value is the resume
     * data.
     */
    - (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes{
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"PDF Download" message:@"Download is resumed successfully" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"ok" style: UIAlertActionStyleDefault handler:nil];
        [controller addAction:action];
        [self presentViewController:controller animated:true completion:nil];
        
    
}
  
    - (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location{
    
    NSLog(location.absoluteString);
    _downloadTask = nil;
    //[self.progressView setProgress: animated:<#(BOOL)#>];
    
}
-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    NSInteger errorReasonNo = [[error.userInfo objectForKey:@"NSURLErrorBackgroundTaskCancelledReasonKey"] integerValue];
    if([error.userInfo objectForKey:@"NSURLErrorBackgroundTaskCancelledReasonKey"] && (errorReasonNo == NSURLErrorCancelledReasonUserForceQuitApplication || errorReasonNo == NSURLErrorCancelledReasonBackgroundUpdatesDisabled)){
        [_progressView setProgress:0 animated:false];
        NSData *resumeData = error.userInfo[NSURLSessionDownloadTaskResumeData];
        if (resumeData) {
            _downloadTask = [self.backgroundSession downloadTaskWithResumeData:resumeData];
            [_downloadTask resume];

            return;
        }
    }
    
    if (error) {
        NSLog(@"%@",error.localizedDescription);
        NSLog(@"%@",error.userInfo);
    }
    _downloadTask = nil;
    [_progressView setProgress:0 animated:true];
}

    // Do any additional setup after loading the view, typically from a nib

- (IBAction)dwnLoadBtn:(id)sender {
    
    if (_downloadTask == nil){
        NSURL *url = [NSURL URLWithString:@"https://scholar.princeton.edu/sites/default/files/oversize_pdf_test_0.pdf"];
        _downloadTask = [self.backgroundSession downloadTaskWithURL:url];
        [_downloadTask resume];
        [_progressView setProgress:0 animated:YES];
    }
}

- (IBAction)pauseBtn:(id)sender {
    if (_downloadTask != nil) {
        [_downloadTask suspend];
    }
}

- (IBAction)playBtn:(id)sender {
    if (_downloadTask != nil){
        [_downloadTask resume];
    }
}

- (IBAction)cancelBtn:(id)sender {
    if (_downloadTask != nil){
        [_downloadTask cancel];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
